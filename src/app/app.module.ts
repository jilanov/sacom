// Angular 2 Modules
import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { AngularFireModule } from 'angularfire2';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocalStorageService, LocalStorageSubscriber } from 'angular2-localstorage/LocalStorageEmitter';

// Router
import { routing, appRoutingProviders } from './app.routing';

// Basic Components
import { ButtonComponent } from './basic_components/button/button.component';
import { HeaderComponent } from './basic_components/header/header.component';
import { NotificationComponent } from './basic_components/notification/notification.component';
import { SearchComponent } from './basic_components/search/search.component';
// import { TableComponent } from './basic_components/table/table.component';

// Components
import { AppComponent } from './app.component';
import { SideMenuComponent } from './components/side_menu/side_menu.component';
import { ViewContainerComponent } from './components/view_container/view_container.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { LoginComponent } from './components/login/login.component';
import { CreateOrderComponent } from './components/create_order/create_order.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DriversComponent } from './components/drivers/drivers.component';
import { GpsTrackingComponent } from './components/gps_tracking/gps_tracking.component';
import { OrderDetailsComponent } from './components/order_details/order_details.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ReportsComponent } from './components/reports/reports.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TripsComponent } from './components/trips/trips.component';
import { PageNotFoundComponent } from './components/page_not_found/page_not_found.component';

// Services
import { DriverService } from './services/driver.service';
import { TripService } from './services/trip.service';
import { NotificationService } from './services/notification.service';
import { OrderService } from './services/order.service';
import { WebSocketService } from './services/web_socket.service';
import { NavigationService } from './services/navigation.service';
import { SessionService } from './services/session.service';
import { LoginService } from './services/login.service';
import { Config } from './config';

export const firebaseConfig = {
    apiKey: "AIzaSyDUHZkBVC5g1x3wzs6YPE8Mpf3jif-vu1w",
    authDomain: "neon-victory-130213.firebaseapp.com",
    databaseURL: "https://neon-victory-130213.firebaseio.com",
    storageBucket: "neon-victory-130213.appspot.com",
    messagingSenderId: "835631843033"
};

@NgModule({
    // Modules & Libs
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AngularFireModule.initializeApp(firebaseConfig),
        routing
    ],
    // Components & Views
    declarations: [ 
        AppComponent,
        ButtonComponent,
        HeaderComponent,
        NotificationComponent,
        SearchComponent,
        // TableComponent,
        SideMenuComponent,
        ViewContainerComponent,
        LoginComponent,
        ContactsComponent,
        CreateOrderComponent,
        DashboardComponent,
        DriversComponent,
        GpsTrackingComponent,
        OrderDetailsComponent,
        OrdersComponent,
        ProfileComponent,
        ReportsComponent,
        SettingsComponent,
        TripsComponent,
        PageNotFoundComponent
    ],
    // Bootstraping
    bootstrap: [ 
        AppComponent 
    ],
    // Services
    providers: [
        Config,
        appRoutingProviders,
        LocalStorageService,
        DriverService,
        LoginService,
        TripService,
        NotificationService,
        OrderService,
        WebSocketService,
        NavigationService,
        SessionService,
    ]
})

export class AppModule { }