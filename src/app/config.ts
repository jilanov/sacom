import { Injectable } from '@angular/core';

@Injectable()
// Will contain all of the urls and constants of the app
export class Config {
    // urls
    public static get oauth():string { return "http://localhost:8042/oauth2/access_token/"; }
    public static get users():string { return "http://localhost:8042/api/users/"; }   
    public static get trucks():string { return "http://localhost:8042/api/trucks/"; }   
    public static get trips():string { return "http://localhost:8042/api/trips/"; }   
    public static get auth_check():string { return "http://localhost:8042/api/auth_check/"; }   
    public static get orders():string { return "http://localhost:8042/api/orders/"; }   
    public static get broker_orders():string { return "http://localhost:8042/api/broker_orders/"; }   
    public static get vehicles():string { return "http://localhost:8042/api/vehicles/"; }   
    public static get broker_vehicles():string { return "http://localhost:8042/api/broker_vehicles/"; }   
    public static get vehicle_types():string { return "http://localhost:8042/api/vehicle_types/"; }   
    public static get companies():string { return "http://localhost:8042/api/companies/"; }   
    public static get contacts():string { return "http://localhost:8042/api/contacts/"; }   
    public static get events():string { return "http://localhost:8042/api/events/"; }   
    public static get event_subscriptions():string { return "http://localhost:8042/api/event_subscriptions/"; }   
    public static get locations():string { return "http://localhost:8042/api/locations/"; }   
    public static get attachments():string { return "http://localhost:8042/api/attachments/"; }   
    public static get zip():string { return "http://localhost:8042/api/zip/"; }

    // tokens
    public static get client_id():string { return "cd5466d60593ec59be77"; }   
    public static get client_secret():string { return "0d432f6e778f517f49496b28db9dce38c2167f4c"; }   
    public static get grant_type():string { return "password"; }
}
