import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

// import { DateComponent } from '../date/date.component';

@Component({
    selector: 'epod-header',
    styleUrls: ['./header.component.css'],
    templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {

    private importButton = {
        class: 'green white-font',
        hasIcon: true,
        radius: true,
        text: 'IMPORT DS'
    };

    private createorderButton = {
        class: '',
        hasIcon: true,
        iconClass: 'glyphicon-flash',
        radius: true,
        text: 'CREATE ORDER'
    };

    private loginButton = {
        class: 'pull-right',
        hasIcon: false,
        radius: false,
        text: 'Log In'
    };

    /**
     * @ngOnInit on init
     */
    public ngOnInit() {
        
    }
}
