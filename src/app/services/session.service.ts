import { Injectable } from '@angular/core';
import {LocalStorage, SessionStorage} from "angular2-localstorage/WebStorage";

@Injectable()

/**
 * @DriverService used on all connections to the back-end for the notifications
 */
export class SessionService {
    @LocalStorage() public scope:String = '';
    @LocalStorage() public token_type:String = '';
    @LocalStorage() public expires_in:String = '';
    @LocalStorage() public access_token:String = '';
    @LocalStorage() public refresh_token:String = '';

    public addTokens(data) {
        this.access_token = data.access_token;
        this.refresh_token = data.refresh_token;
        this.scope = data.scope;
        this.token_type = data.token_type;
        this.expires_in = data.expires_in;
    }

    public getAccessToken() {
        return this.access_token;
    }

    public getRefreshToken() {
        return this.refresh_token;
    }

    public getExpiresIn() {
        return this.expires_in;
    }

    public getTokenType() {
        return this.token_type;
    }

    public getScope() {
        return this.scope;
    }

    public checkAndLog() {
        // TODO: CHECK IS THE TOKEN EXPIRED
        if((this.access_token.length > 5) && (this.access_token !== undefined)) {
            return true;
        }
        return false;
    }
    // cheat the system.. think about how it will be better later
    constructor() {}
}
