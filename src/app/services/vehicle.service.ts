import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Config } from '../config';
import { SessionService } from '../services/session.service';

@Injectable()

/**
 * @VehicleService used on all connections to the back-end for the vehicles
 */
export class VehicleService {

    /**
    * @postHeaders {Headers} 
    * @postRequestOptions {RequestOptions}
    * @getRequestOptions {RequestOptions}
    */
    private postHeaders: Headers = new Headers;
    private getHeaders: Headers = new Headers;
    private postRequestOptions: RequestOptions = new RequestOptions;
    private getRequestOptions: RequestOptions = new RequestOptions;

    constructor( private http: Http ) {
        
        // Set post headers what to expect and what to send
        this.postHeaders.append('Accept', 'application/json');
        this.getHeaders.append('Accept', 'application/json');

        this.postHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
        this.postHeaders.append('Authorization', 'Bearer ' + this.SessionService.getAccessToken());
        this.getHeaders.append('Authorization', 'Bearer ' + this.SessionService.getAccessToken());

        // Set post request options headers
        this.postRequestOptions.headers = this.postHeaders;
        this.getRequestOptions.headers = this.getHeaders;
    }

    /**
    * @getVehicles get all the vehicles
    * @return {Array} all vehicles
    */
    public getVehicles() {
        return this.http.get( Config.vehicles, this.getRequestOptions ).map( res => res.json() );
    }
}
