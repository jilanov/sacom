import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';

@Injectable()

/**
 * @WebSocketService used to fetch the push notifications from the back-end
 */
export class WebSocketService {
    // the observable data of the firebase
    items: FirebaseObjectObservable<any>;
    firebase: Object;
    /**
    * @af angular fire instance
    */
    constructor(af: AngularFire) {
        console.log('firebase: ' + this.items);
        this.items = af.database.object('/item');
        this.firebase = af;
    }
    // possible options set/update/remove
    removeData() {
        const promise = this.firebase['database'].object('/item').remove();
        promise
          .then(_ => console.log('success'))
          .catch(err => console.log(err, 'You dont have access!'));
    }
}
