import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { OAuthService } from 'angular2-oauth2/oauth-service';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Config } from '../config';

@Injectable()

/**
 * @InitService used on initialization of the app to fetch the data from the back-end
 */
export class LoginService {

    private postHeaders: Headers = new Headers;
    private postRequestOptions: RequestOptions = new RequestOptions;

    constructor(public http: Http, private sessionService: SessionService) {
        this.postHeaders.append('Accept', 'application/json');
        this.postHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
        this.postRequestOptions.headers = this.postHeaders;
    }
    /**
    * @login login to the app
    * @email {String}
    * @password {String}
    */
    public login(formInfo) {
        var email = formInfo.email;
        var password = formInfo.password;
        // TODO: The oauth service of angular doesnt work well so we make a custom made one
        var loginData = 'username=' + encodeURIComponent(email) 
            + '&password=' + encodeURIComponent(password) 
            + '&client_id=' + encodeURIComponent(Config.client_id) 
            + '&client_secret=' + encodeURIComponent(Config.client_secret) 
            + '&grant_type=' + encodeURIComponent(Config.grant_type);
        console.log('Login data: ' + email + ' ' + password + ' is connecting to: ' + Config.oauth + ' using ' + Config.client_id + ' ' + Config.client_secret);
        return this.http.post(Config.oauth, loginData, this.postRequestOptions).map(res => res.json());
    }
    
    public logoff() {
        
    }
}
