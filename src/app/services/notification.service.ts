import { Injectable } from '@angular/core';

@Injectable()

/**
 * @DriverService used on all connections to the back-end for the notifications
 */
export class NotificationService {
    /**
    * @extractDates get the dates to the array
    * @selectedDatesArray {Array} contains all dates
    * @postNow {Boolean} Should we post now
    */
    private extractDates(selectedDatesArray, postNow) {
        var strategy = {
            'start_date': '',
            'end_date': '',
            'post_now': false,
        };

        if (selectedDatesArray.length > 0) {
            var lastArrayIndex = selectedDatesArray.length - 1;

            strategy.start_date = selectedDatesArray[0].date;
            strategy.end_date = selectedDatesArray[lastArrayIndex].date;
            strategy.post_now = postNow;
        }

        return strategy;
    }
}
