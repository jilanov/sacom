// Angular 2 Modules
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Views
import { ContactsComponent } from './components/contacts/contacts.component';
import { CreateOrderComponent } from './components/create_order/create_order.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DriversComponent } from './components/drivers/drivers.component';
import { GpsTrackingComponent } from './components/gps_tracking/gps_tracking.component';
import { OrderDetailsComponent } from './components/order_details/order_details.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ReportsComponent } from './components/reports/reports.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TripsComponent } from './components/trips/trips.component';
import { PageNotFoundComponent } from './components/page_not_found/page_not_found.component';

const appRoutes: Routes = [
  { 
    path: '', 
    component: LoginComponent,
    data: {
      title: 'Login'
    }
  },
  { 
    path: 'dashboard', 
    component: DashboardComponent,
    data: {
      title: 'Dashboard'
    }
  },
  { 
    path: 'contacts', 
    component: ContactsComponent,
    data: {
      title: 'Contacts'
    }
  },
  { 
    path: 'create-order', 
    component: CreateOrderComponent,
    data: {
      title: 'Create Order'
    }
  },
  { 
    path: 'drivers', 
    component: DriversComponent,
    data: {
      title: 'Drivers'
    }
  },
  { 
    path: 'gps-tracking', 
    component: GpsTrackingComponent,
    data: {
      title: 'Gps Tracking'
    }
  },
  { 
    path: 'order-details', 
    component: OrderDetailsComponent,
    data: {
      title: 'Order Details'
    }
  },
  { 
    path: 'orders', 
    component: OrdersComponent,
    data: {
      title: 'Orders'
    }
  },
  { 
    path: 'profile', 
    component: ProfileComponent,
    data: {
      title: 'Profile'
    }
  },
  { 
    path: 'reports', 
    component: ReportsComponent,
    data: {
      title: 'Reports'
    }
  },
  { 
    path: 'settings', 
    component: SettingsComponent,
    data: {
      title: 'Settings'
    }
  },
  { 
    path: 'trips', 
    component: TripsComponent,
    data: {
      title: 'Trips'
    }
  },
  { 
    path: '**', 
    component: PageNotFoundComponent
  }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);