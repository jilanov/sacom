import { Component, OnInit, Inject } from '@angular/core';
import { SessionService } from './services/session.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    constructor(
        private sessionService: SessionService,
        private router: Router
    ) {};
    /**
    * @ngOnInit on init
    */
    public ngOnInit() {}
}
