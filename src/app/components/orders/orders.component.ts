import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { OrderService } from '../../services/order.service';

@Component({
    selector: 'orders',
    styleUrls: ['./orders.component.css'],
    templateUrl: './orders.component.html'
})

export class OrdersComponent implements OnInit {

    constructor(
        private ordersService: OrderService
    ) {}

    /**
     * @ngOnInit contains the data of the login form
     */
    private orderObj: Array <any> = [];

    /**
     * @getOrders when submiting form it logins to the API
     */
    private getOrders() {
        this.ordersService.getOrders().subscribe(
            data => {
                this.orderObj = data.results;
            },
            err => {
                err = JSON.parse(err._body);
                console.log('Error: ' + err);
            }
       );
    };

    /**
     * @ngOnInit on init
     */
    public ngOnInit() {
        this.getOrders();
    }


}
