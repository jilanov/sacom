import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { DriverService } from '../../services/driver.service';

@Component({
    selector: 'drivers',
    styleUrls: ['./drivers.component.css'],
    templateUrl: './drivers.component.html'
})

export class DriversComponent implements OnInit {

    constructor(
        private driverService: DriverService
    ) {}

    /**
     * @ngOnInit contains the data of the login form
     */
    private driverObj: Array <any> = [];

    /**
     * @getOrders when submiting form it logins to the API
     */
    private getDrivers() {
        this.driverService.getDrivers().subscribe(
            data => {
                this.driverObj = data.results;
            },
            err => {
                err = JSON.parse(err._body);
                console.log('Error: ' + err);
            }
       );
    };

    /**
     * @ngOnInit on init
     */
    public ngOnInit() {
        this.getDrivers();
    }
}