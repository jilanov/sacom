import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { LoginService } from '../../services/login.service';
import { SessionService } from '../../services/session.service';
import { Router } from '@angular/router';

@Component({
    selector: 'trips',
    styleUrls: ['./login.component.css'],
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    constructor(
        private loginService: LoginService,
        private sessionService: SessionService,
        private router: Router
    ) {};
        
    /**
     * @ngOnInit contains the data of the login form
     */
    private loginForm: Object = {
        email: '',
        password: ''
    };

    /**
     * @login when submiting form it logins to the API
     */
    private login() {
        this.loginService.login(this.loginForm).subscribe(
            data => {
                this.sessionService.addTokens(data);
                this.router.navigate(['/orders']);
            },

            err => {
                err = JSON.parse(err._body);
                console.log('Error: ' + err);
            }
        );
    };

    /**
     * @ngOnInit on init
     */
    public ngOnInit() {
        // check are the session tokens provided and if they are not or not up to date we send the person to log in screen
        if(this.sessionService.checkAndLog()) {
            // go to dashboard screen if we are into the base login screen
            this.router.navigate(['/orders']);
        }
    };
}
