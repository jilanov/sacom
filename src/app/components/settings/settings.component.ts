import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

//import { DateComponent } from '../date/date.component';

@Component({
    selector: 'settings',
    styleUrls: ['./settings.component.css'],
    templateUrl: './settings.component.html'
})

export class SettingsComponent implements OnInit {
    @Input()
    postOptions: Object;

    @Output()
    cPayChanged = new EventEmitter();

    private showOverlay = false;

    /**
     * @ngOnInit on init
     */
    public ngOnInit() {

    }
}
