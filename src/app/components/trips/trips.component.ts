import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { TripService } from '../../services/trip.service';

@Component({
    selector: 'trips',
    styleUrls: ['./trips.component.css'],
    templateUrl: './trips.component.html'
})

export class TripsComponent implements OnInit {

    constructor(
        private tripService: TripService
    ) {}

    /**
     * @ngOnInit contains the data of the login form
     */
    private tripObj: Array <any> = [];

    /**
     * @getOrders when submiting form it logins to the API
     */
    private getTrips() {
        this.tripService.getTrips().subscribe(
            data => {
                this.tripObj = data.results;
            },
            err => {
                err = JSON.parse(err._body);
                console.log('Error: ' + err);
            }
       );
    };

    /**
     * @ngOnInit on init
     */
    public ngOnInit() {
        this.getTrips();
    }
}
