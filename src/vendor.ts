// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

// RxJS
import 'rxjs';

// Thrid party libs
import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';
import { ToasterModule, ToasterService } from 'angular2-toaster/angular2-toaster';

// Epod SPA styles
require('./sass/style.scss');